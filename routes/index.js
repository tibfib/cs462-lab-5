const router = require('express').Router();
const crypto = require('crypto');
const axios = require('axios');

const id = crypto.randomBytes(16).toString('hex');

let nodes = [],
    me;

if (process.env.AUTO) {
    nodes = [
        {
            id: 'node1',
            url: 'http://localhost:3001/api/chat'
        },
        {
            id: 'node2',
            url: 'http://localhost:3002/api/chat'
        },
        {
            id: 'node3',
            url: 'http://localhost:3003/api/chat'
        }
        // {
        //     id: 'node4',
        //     url: 'http://localhost:3004/api/chat'
        // }
    ];

    let meIndex = nodes.findIndex(node => node.url.includes(process.env.PORT));
    me = nodes.splice(meIndex, 1)[0];
} else {
    me = {
        id: `node${process.env.PORT}`,
        url: `http://localhost:${process.env.PORT}/api/chat`
    };
    nodes = [];
}

let messages = [
    {
        rumor: {
            messageId: `${id}:1`,
            originator: `Node ${process.env.PORT}`,
            text: 'My first message'
        },
        endpoint: me.url
    },
    {
        rumor: {
            messageId: `${id}:2`,
            originator: `Node ${process.env.PORT}`,
            text: 'My second message'
        },
        endpoint: me.url
    }
];

function getRandomItem(array) {
    return array[Math.floor(Math.random() * array.length)];
}

// RECEIVING MESSAGES
router.post('/new_message', function(req, res) {
    let text = req.body.text;
    messages.push({
        rumor: {
            messageId: `${id}:${getWant(messages).want[id] + 1}`,
            originator: `Node ${process.env.PORT}`,
            text: text
        },
        endPoint: me.url
    });
});

router.post('/new_node', function(req, res) {
    nodes.push({
        id: req.body.id,
        url: req.body.url
    });

    res.json(nodes);
});

router.get('/chat', function(req, res) {
    res.json({ want: getWant(messages), messages: messages });
});

router.post('/chat', function(req, res) {
    console.log('got message from ', req.body.endpoint);

    const t = req.body;

    if (t.rumor) {
        if (!messages.find(m => m.rumor.messageId === t.rumor.messageId)) {
            console.log('storing new message');
            // store if we don't have it already
            messages.push(t);
            if (!nodes.find(node => node.endpoint === t.endpoint)) {
                let { id } = splitMessageId(t.rumor.messageId);
                nodes.push({
                    id: id,
                    url: t.endpoint
                });
            }
        }
    } else if (t.want) {
        messages
            .filter(message => {
                let { id: hasId, sequence: hasSequence } = splitMessageId(message.rumor.messageId);
                // filter out messages that aren't in the want at all OR where the want has a lower id than we have.
                return !t.want[hasId] || t.want[hasId] < hasSequence;
            })
            .forEach(message => send(t.endpoint, message));
    }
});

// PROPAGATING
function splitMessageId(messageId) {
    let [id, sequence] = messageId.split(':');
    return { id, sequence: +sequence };
}
function getWant(messages) {
    return {
        want: messages
            .slice()
            .sort((a, b) => {
                sequenceNumberA = splitMessageId(a.rumor.messageId).sequence;
                sequenceNumberB = splitMessageId(b.rumor.messageId).sequence;
                return sequenceNumberA - sequenceNumberB;
            })
            .reduce((result, message) => {
                let { id, sequence } = splitMessageId(message.rumor.messageId);
                if (!result[id]) result[id] = 0;
                if (sequence === result[id] + 1) result[id] = sequence;

                return result;
            }, {}),
        endpoint: me.url
    };
}

function send(endpoint, message) {
    console.log('sending message to', endpoint);
    axios.post(endpoint, message).catch(er => er);
}
function propagate() {
    let peer = getRandomItem(nodes);
    if (!peer) return;
    let message = Math.random() > 0.5 ? getRandomItem(messages) : getWant(messages);
    send(peer.url, message);
}

const N = 1000;
setInterval(propagate, N);

module.exports = router;
